package org.example;

import static org.junit.Assert.assertTrue;

import org.junit.Test;
import java.util.UUID;
import java.util.concurrent.Semaphore;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }

    @Test
    public void test()
    {
        for ( int i = 0; i < 10; i++ ){
            System.out.println("\u001B[0m"+UUID.randomUUID().toString().substring(27));
        }
    }

    @Test
    public void testSemaphore() {
        Semaphore semaphore = new Semaphore(1);

        if(semaphore.tryAcquire()){
            System.out.println(semaphore);
        }

        if(semaphore.tryAcquire()){
            System.out.println(semaphore);
        }
        semaphore.release();
        semaphore.release();
        System.out.println(semaphore);
        System.out.println(semaphore.availablePermits());

    }

    //@Test
    //public void test1(){
    //    Date date = new Date(1435587250000L);
    //    System.out.println(date);
    //}
}
