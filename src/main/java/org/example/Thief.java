package org.example;

import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadLocalRandom;

public class Thief extends Person {

    Backpack backpack = new Backpack().setRandomMaxWeight();
    int receivedItemWeight = 0;
    int itemCounter = 0;

    public Boolean getItemFromRoom(ConcurrentHashMap<String,Item> itemsFromRoom) {
        if(!itemsFromRoom.isEmpty()){
            while(receivedItemWeight < backpack.getMaxWeight()&&!itemsFromRoom.isEmpty()){
                Map.Entry<String,Item> itemWithMaxPrice = null;
                for(Map.Entry<String,Item> entry : itemsFromRoom.entrySet()){
                    if(itemWithMaxPrice == null|| entry.getValue().getPrice() >= itemWithMaxPrice.getValue().getPrice()){
                        itemWithMaxPrice = entry;
                    }
                }
                if(itemWithMaxPrice!=null) {
                    receivedItemWeight = receivedItemWeight + itemWithMaxPrice.getValue().getWeight();
                    backpack.getItemsInBackpack().put(itemWithMaxPrice.getKey(), itemWithMaxPrice.getValue());
                    itemsFromRoom.remove(itemWithMaxPrice.getKey(), itemWithMaxPrice.getValue());
                    itemCounter += 1;

                }
            }
            return true;
        }
        return false;
    }

    public Boolean tryAcquireSemaphore(GeneralSemaphore semaphore){
        if(semaphore!=null){
            if(semaphore.getSemaphoreOwner().availablePermits() ==6){
                semaphore.getSemaphoreOwner().tryAcquire(6);
            }
            return (semaphore.getSemaphoreThief().tryAcquire());
        } else return false;
    }

    public Boolean increaseStealCounter(){
        Statistics statistics = null;
        while(statistics == null){
            statistics = Statistics.getInstance();
        }
        statistics.setStolenItemCount(statistics.getStolenItemCount()+itemCounter);
        return true;
    }

    @Override
    public Boolean call() throws InterruptedException {
        Thread.sleep(ThreadLocalRandom.current().nextInt(0, 500));
        GeneralSemaphore semaphore = null;
        Boolean checkTry = false;

        while(semaphore == null){
            semaphore = GeneralSemaphore.getInstance();
        }
        while(!checkTry){
        checkTry = tryAcquireSemaphore(semaphore);
        }
        //System.out.println("\u001B[37m" +"semaphore thief "+semaphore.getSemaphoreThief().availablePermits());
        //System.out.println("\u001B[37m" +"semaphore owner "+ semaphore.getSemaphoreOwner().availablePermits());
        System.out.println("\u001B[35m" + "Thief"+Thread.currentThread().getName().substring(14)+" in room");

        if(getItemFromRoom(Room.getInstance().getItemsFromRoom())){
                Thread.sleep(500);
            System.out.println("\u001B[35mThief"+Thread.currentThread().getName().substring(14)+" get item");
            for(Map.Entry<String, Item> item : backpack.getItemsInBackpack().entrySet()){
                System.out.println("price:" + item.getValue().getPrice() + " weight:" + item.getValue().getWeight());
            }
        } else{
            System.out.println("\u001B[35mThief"+Thread.currentThread().getName().substring(14)+" don't get item");
        }
        semaphore.getSemaphoreThief().release();
        //System.out.println("\u001B[37m" +"semaphore thief "+semaphore.getSemaphoreThief().availablePermits());

        if(semaphore.getSemaphoreOwner().availablePermits() == 0){
            semaphore.getSemaphoreOwner().release(6);
            //System.out.println("\u001B[37m" +"semaphore owner "+ semaphore.getSemaphoreOwner().availablePermits());
        }
        increaseStealCounter();
        return true;
    }
}
