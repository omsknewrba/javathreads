package org.example;

import java.util.concurrent.Semaphore;

public class GeneralSemaphore {

    private static GeneralSemaphore instance;

    Semaphore semaphoreThief= new Semaphore(1);
    Semaphore semaphoreOwner= new Semaphore(6);

    private GeneralSemaphore(){
    }

    public static synchronized GeneralSemaphore getInstance() {
        if (null == instance) {
            instance = new GeneralSemaphore();
        }
        return instance;
    }

    public Semaphore getSemaphoreThief() {
        return semaphoreThief;
    }

    public void setSemaphoreThief(Semaphore semaphoreThief) {
        this.semaphoreThief = semaphoreThief;
    }

    public Semaphore getSemaphoreOwner() {
        return semaphoreOwner;
    }

    public void setSemaphoreOwner(Semaphore semaphoreOwner) {
        this.semaphoreOwner = semaphoreOwner;
    }
}
