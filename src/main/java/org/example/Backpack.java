package org.example;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class Backpack {

    private int maxWeight;

    private Map<String,Item> itemsInBackpack = new HashMap<>();

    public Backpack(int maxWeight) {
        this.maxWeight = maxWeight;
    }

    public Backpack setRandomMaxWeight(){
        maxWeight = ThreadLocalRandom.current().nextInt(20, 60);
        return this;
    }

    public Backpack setRandomItems(){
        int itemsWeight = 0;
        while(itemsWeight < maxWeight){
        String itemId = UUID.randomUUID().toString().substring(27);
        int price = ThreadLocalRandom.current().nextInt(500, 3000);
        int weight = ThreadLocalRandom.current().nextInt(10, 30);
        Item item = new Item(weight, price);
        itemsInBackpack.put(itemId,item);
        itemsWeight += weight;
        }
        return this;
    }

    public Backpack() {
    }

    public int getMaxWeight() {
        return maxWeight;
    }

    public void setMaxWeight(int maxWeight) {
        this.maxWeight = maxWeight;
    }

    public Map<String, Item> getItemsInBackpack() {
        return itemsInBackpack;
    }

    public void setItemsInBackpack(Map<String, Item> itemsInBackpack) {
        this.itemsInBackpack = itemsInBackpack;
    }
}
