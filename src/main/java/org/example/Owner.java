package org.example;

import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

public class Owner extends Person {

    private Backpack backpack = new Backpack().setRandomMaxWeight()
                                      .setRandomItems();
    int receivedItemWeight = 0;
    int itemCounter = 0;

    public Boolean putItemInRoom(Room room) {
        System.out.println("\u001B[33mOwner"+Thread.currentThread().getName().substring(14)+" put item");
            for(Map.Entry<String, Item> item : backpack.getItemsInBackpack().entrySet()){
                if(room.getItemsFromRoom().putIfAbsent(item.getKey(), item.getValue())==null){
                    System.out.println("price: " + item.getValue().getPrice() + "weight: " + item.getValue().getWeight());
                    itemCounter += 1;
                }
            }
            backpack.getItemsInBackpack().clear();
        return true;
    }

    public Boolean tryAcquireSemaphore(GeneralSemaphore semaphore) {
        if(semaphore.getSemaphoreThief().availablePermits() == 1) {
            semaphore.getSemaphoreThief().tryAcquire();
        }
        return (semaphore.getSemaphoreOwner().tryAcquire());
    }

    public Boolean increasePutCounter(){
        Statistics statistics = null;
        while(statistics == null){
            statistics = Statistics.getInstance();
        }
        statistics.setPutItemCount(statistics.getPutItemCount()+itemCounter);
        return true;
    }

    @Override
    public Boolean call() throws InterruptedException {
        Thread.sleep(ThreadLocalRandom.current().nextInt(0, 500));

        GeneralSemaphore semaphore = null;
        Boolean checkTry = false;
        while(semaphore ==null){

            semaphore = GeneralSemaphore.getInstance();
        }
        while(!checkTry){
            checkTry = tryAcquireSemaphore(semaphore);
        }
        System.out.println("\u001B[33mOwner"+Thread.currentThread().getName().substring(14)+" in room");
        //System.out.println("\u001B[37msemaphore thief "+semaphore.getSemaphoreThief().availablePermits());
        //System.out.println("\u001B[37msemaphore owner "+ semaphore.getSemaphoreOwner().availablePermits());

        if (putItemInRoom(Room.getInstance())) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace(System.out);
            }
            if(semaphore.getSemaphoreThief().availablePermits()==0 && semaphore.getSemaphoreOwner().availablePermits()==5){
            semaphore.getSemaphoreThief().release();
            }
            semaphore.getSemaphoreOwner().release();
            increasePutCounter();
            //System.out.println("\u001B[37msemaphore thief "+semaphore.getSemaphoreThief().availablePermits());
            //System.out.println("\u001B[37msemaphore owner "+ semaphore.getSemaphoreOwner().availablePermits());
        }
        return true;
    }
}

