package org.example;

import java.util.concurrent.ConcurrentHashMap;

public class Room {

    private static Room instance;

    private ConcurrentHashMap<String, Item> itemsFromRoom;

    public static Room getInstance(){
        if (null == instance) {
            instance = new Room();
        }
        return instance;
    }

    private Room() {
        itemsFromRoom = new ConcurrentHashMap<>();
    }

    public ConcurrentHashMap<String, Item> getItemsFromRoom() {
        return itemsFromRoom;
    }

    public void setItemsFromRoom(ConcurrentHashMap<String, Item> itemsFromRoom) {
        this.itemsFromRoom = itemsFromRoom;
    }

}
