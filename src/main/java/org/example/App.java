package org.example;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

public class App
{
    static Semaphore semaphoreThief= new Semaphore(1);
    static Semaphore semaphoreOwner= new Semaphore(6);

    public static void main( String[] args ) throws InterruptedException, ExecutionException {

        int threadPoolSize = 20;
        int threadsQuantity = 10;
        CountDownLatch countDownLatch = new CountDownLatch(threadPoolSize);
        ExecutorService executorService = Executors.newFixedThreadPool(threadPoolSize);
        List<Person> persons = new ArrayList<>();

        for(int i = 0; i < threadsQuantity; i++){
            Owner owner = new Owner();
            persons.add(owner);
        }
        for(int i = 0; i < threadsQuantity; i++){
            Thief thief = new Thief();
            persons.add(thief);
        }
        System.out.println("Threads created");

        List<Future<Boolean>> personFutures = executorService.invokeAll(persons);

        for(Future<Boolean> future : personFutures){
            if(future.get()){
                countDownLatch.countDown();
            }
        }

      countDownLatch.await(10L,TimeUnit.SECONDS);

        Thread.sleep(500);
        Statistics statistics = null;
        while(statistics == null) {
            statistics = Statistics.getInstance();
        }
        statistics.viewLeftItems();
        System.out.println("Put items: " + statistics.getPutItemCount());
        System.out.println("Stolen items: " + statistics.getStolenItemCount());

        executorService.shutdown();
    }
}
