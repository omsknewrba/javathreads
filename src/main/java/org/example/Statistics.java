package org.example;

import java.util.Map;

public class Statistics {

    private static Statistics instance;
    private volatile Integer putItemCount = 0;
    private volatile Integer stolenItemCount = 0;

    private Statistics() {
    }

    public static synchronized Statistics getInstance() {
        if (null == instance) {
            instance = new Statistics();
        }
        return instance;
    }

    public Boolean viewLeftItems(){
        Room room = null;
        while(room== null){
            room = Room.getInstance();
        }
        if(room.getItemsFromRoom().isEmpty()) {
            System.out.println("\u001B[32mRoom is empty");
        }
        else{
            System.out.println("\u001B[32mItems left: " + room.getItemsFromRoom().size());
            for(Map.Entry<String,Item> entry : room.getItemsFromRoom().entrySet()){
                System.out.println(entry.getKey() + " - price: " + entry.getValue().getPrice() + ", weight: " + entry.getValue().getWeight());
            }
        }
        return true;
    }

    public Integer getPutItemCount() {
        return putItemCount;
    }

    public void setPutItemCount(Integer putItemCount) {
        this.putItemCount = putItemCount;
    }

    public Integer getStolenItemCount() {
        return stolenItemCount;
    }

    public void setStolenItemCount(Integer stolenItemCount) {
        this.stolenItemCount = stolenItemCount;
    }
}
